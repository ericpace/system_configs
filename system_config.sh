# ---------------------------------
# Configuration of KDE installation
# ---------------------------------


# 0. System update
sudo apt update
sudo apt upgrade


# 1. Spotify
# 1.1 Update hosts file for Spotify
sudo nano /etc/hosts
# 1.2 Install
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update
sudo apt-get install spotify-client


# 2. Setup for ProtonVPN
# 2.1 Check the following are installed:
which openvpn
which python
which dialog
which wget
# 2.2 If not, install:
sudo apt update
sudo apt install openvpn
wudo apt install dialog
sudo apt install dialog
# 2.3 Get the files
sudo wget -O protonvpn-cli.sh https://raw.githubusercontent.com/ProtonVPN/protonvpn-cli/master/protonvpn-cli.sh
sudo chmod +x protonvpn-cli.sh
sudo ./protonvpn-cli.sh --install
# 2.4 Configure the installation
#     (Need credentials from ProtonVPN website)
sudo pvpn -init
# 2.5 Connect (use -f for fastest connection)
sudo pvpn -c


# 3. TeXLive installation
# 3.1 Download texlive
cd Downloads/install-tl-20190227/
# 3.2 Run installer
sudo ./install-tl
# 3.3 Add to PATH in .profile
sudo nano ~/.profile 


# 4. Install Git
sudo apt install git-all


# 5. Install Pycharm
sudo snap install pycharm-community --classic


# 6. Install Sublime
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text


# 7. Update LibreOffice
# 7.1 Remove existing installation
sudo apt remove --purge libreoffice-core
# 7.2 Install latest via Snap
sudo snap install libreoffice
# 7.3 Possibly need to update start menu shortcuts


# 8. Install Mendeley
# 8.1 Download deb package
sudo dpkg -i mendeleydesktop_1.19.4-stable_amd64.deb
# 8.2 If encounter errors (such as no gconf2)
sudo apt --fix-broken install
# 8.3 If broken packages are fixed, re-run dpkg


# 9. Install Python
sudo apt update
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.7


# 10. Install Python3-pip
sudo apt install python3-pip

# 11. Goodies
sudo apt install htop
sudo apt install neofetch


# Install Powerline
pip3 install powerline-status

wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf

sudo mv PowerlineSymbols.otf /usr/share/fonts/X11/misc
sudo mkdir ~/.config/fontconfig/conf.d
sudo mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/

# Find the location of powerline installation
pip3 show powerline-status
# From the above command you should get a location: {path}

# Add the following to ~/.bashrc
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /home/eric/.local/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh

# You might need to install this for the daemon
sudo apt install powerline


# Install Pygmentize
sudo apt install python-pygments


# Install GIMP
sudo add-apt-repository ppa:otto-kesselgulasch/gimp
sudo apt update
sudo apt install gimp

